var canvas = document.getElementById('bgCanvas');
var ctx = canvas.getContext('2d');
var particles = [];
var particleCount = 300;

var timer = null;

var imgJellyfish = new Image();
imgJellyfish.src = "img/jellyfish.png";

for(var i=0; i<particleCount;i++)
  particles.push(new particle());

function particle() {
  this.x = canvas.width + Math.random() * 300;
  this.y = Math.random() * canvas.height;
  this.speed = 1 + Math.random();
  this.radius = Math.random() * 3;
  this.opacity = (Math.random() * 100) / 1000;
}


function loop() {
  //setInterval(draw, 5);
  
  cancelAnimationFrame(timer);
  timer = requestAnimationFrame(loop);
}

function draw() {
  ctx.clearRect(0,0,canvas.width,canvas.height);
  
  ctx.globalCompositeOperation = 'lighter';
  for(var i=0; i<particles.length; i++) {
    var p = particles[i];
    ctx.beginPath();
    ctx.fillStyle = 'rgba(255,255,255,' + p.opacity + ')';
    ctx.arc(p.x, p.y, p.radius, 0, Math.PI*2, false);
    ctx.fill();
    p.x -= p.speed;
    if(p.x <= -10)
      particles[i] = new particle();
  }
}

loop();
/*
$(window).scroll(function() {
  var scrollTop = $(".navbar").scrollTop();
  if (scrollTop > 50) {
    isPaused = true;
  } else {
    if (isPaused) {
      isPaused = false;
      loop();
    }
  }
});
*/
